# Cmc Calculator

This is a companion code for the presentation on 201120 during DPTO. 

This code is by no means perfect; it would require some refactoring etc. It does follow the principles of "start from data" and shows how you can grow the project while your understanding of the domain grows. Due to the presentation being limited to 40 minutes, it stops here, without separating stuff or refactoring anything sensible.