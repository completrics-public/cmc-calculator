﻿using ConsoleApp2.Cmc;
using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            string mana = Console.ReadLine();

            int result = new Calculator().ManaToCmc(mana);

            Console.WriteLine("Current mana:" + result.ToString());
        }
    }
}
