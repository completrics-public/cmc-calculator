﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp2.Cmc
{
    public class Calculator
    {
        public int ManaToCmc(string mana)
        {
            // "2wwuu" -> ["2", "wwuu"]
            // ConvertColorless("2") -> 2
            // ConvertSpecific("wwuu") -> 4
            // return 2+4

            (int colorlessMana, int specificMana) = (0, 0);

            if (mana.Contains("{"))         // We are dealing with Extended structure
            {
                (colorlessMana, specificMana) = CalculateExtended(mana);
            }
            else                            // We are dealing with simplified structure
            {
                (colorlessMana, specificMana) = CalculateSimplified(mana);
            }

            return colorlessMana + specificMana;
        }

        private (int colorlessMana, int specificMana) CalculateExtended(string mana)
        {
            Regex regex = new Regex(@"{(\d+)}");
            string colorlessPart = regex.Match(mana).Groups[1].Value;
            int colorlessMana = ConvertColorless(colorlessPart);

            int specificMana = 0;
            if(colorlessMana == 0)
            {
                //"{B/W}{B/W}{B/W}{B/W}" -> "{, {, {, {"
                specificMana = mana.Where(m => m == '{').Count();
            }
            else
            {
                // {2}{B/R}{B/R} -> {2}, {B/R}
                // {B/R}{B/R} -> Count("{")
                int startOfSpecific = mana.IndexOf("}");
                string specificManaPart = mana.Substring(startOfSpecific + 1);
                specificMana = specificManaPart.Where(m => m == '{').Count();
            }

            return (colorlessMana, specificMana);
        }

        public (int colorlessMana, int specificMana) CalculateSimplified(string mana)
        {
            Regex regex = new Regex(@"(\d*)(\w*)");
            var result = regex.Match(mana);
            string colorlessPart = result.Groups[1].Value;
            string specificPart = result.Groups[2].Value;

            return (ConvertColorless(colorlessPart), ConvertSpecific(specificPart));
        }

        public int ConvertColorless(string colorless)
        {
            if (string.IsNullOrEmpty(colorless))
            {
                return 0;
            }
            else
            {
                return int.Parse(colorless);
            }
        }

        public int ConvertSpecific(string specific)
        {
            return specific.Length;
        }
    }
}
