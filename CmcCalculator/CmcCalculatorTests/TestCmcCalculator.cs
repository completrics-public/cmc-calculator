using ConsoleApp2.Cmc;
using NUnit.Framework;

namespace NUnitTestProject1
{
    public class TestCmcCalculator
    {
        [Test]
        public void ConvertColorlessWorksFor_3()
        {
            // Given
            string crystalline = "3";
            int expected = 3;

            // When
            int actual = new Calculator().ConvertColorless(crystalline);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ConvertSpecificWorksFor_BBB()
        {
            // Given
            string underworld = "bbb";
            int expected = 3;

            // When
            int actual = new Calculator().ConvertSpecific(underworld);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ConvertManaWorksFor_2WWUU()
        {
            // Given
            string mana = "2wwuu";  // dream trawler
            int expected = 6;

            // When
            int actual = new Calculator().ManaToCmc(mana);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [TestCase("2wwuu", 6)]                  // dream trawler, standard
        [TestCase("3", 3)]                      // crystalline giant, standard
        [TestCase("bbb", 3)]                    // underworld dreams, standard
        [TestCase("10gg", 12)]                  // ghalta, standard
        [TestCase("0", 0)]                      // black lotus, standard
        [TestCase("0b", 1)]                     // parsable error, standard
        [TestCase("{3}{B/R}{B/R}", 5)]          // obosh, extended
        [TestCase("{B/W}{B/W}{B/W}{B/W}", 4)]   // arcanist owl, extended
        [TestCase("{3}", 3)]                    // crystalline giant, extended
        [TestCase("{B}{B}{B}", 3)]              // underworld dreams, extended
        public void ConvertManaTest(string mana, int expected)
        {
            // When
            int actual = new Calculator().ManaToCmc(mana);

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}